import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Book } from '../../models/book.model';
import { OnInit } from '@angular/core';
import { FormControl, ReactiveFormsModule } from '@angular/forms';
import { Store, select } from '@ngrx/store';
import { addBook, deleteBook, getBooks } from '../../store/books.actions';
import { selectBookById, selectBooksList } from '../../store/books.selectors';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-books',
  standalone: true,
  imports: [CommonModule, ReactiveFormsModule],
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.css'],
})
export class BooksComponent implements OnInit {
  books: Book[] = [];
  searchedBook: Book | undefined;

  idFormControl = new FormControl();
  titleFormControl = new FormControl();
  getByIdControl = new FormControl();
  deleteByIdControl = new FormControl();

  constructor(private store: Store) {}

  ngOnInit(): void {
    this.initDispatch();
    this.initSubscriptions();
  }

  addBook() {
    this.store.dispatch(
      addBook({
        book: {
          id: this.idFormControl.value,
          title: this.titleFormControl.value,
        },
      })
    );

    this.idFormControl.reset();
    this.titleFormControl.reset();
  }

  searchById() {
    const id = this.getByIdControl.value;
    this.store.select(selectBookById(id)).subscribe((book) => (this.searchedBook = book));
  }

  deleteById() {
    const id = this.deleteByIdControl.value;
    this.store.select(selectBookById(id)).subscribe((b) => {
      if(b)
        this.store.dispatch(deleteBook({ book: b }));
    });
  }

  private initDispatch() {
    this.store.dispatch(getBooks());
  }

  private initSubscriptions() {
    this.store.pipe(select(selectBooksList)).subscribe((res) => (this.books = res));
  }
}
