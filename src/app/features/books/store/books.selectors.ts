import { createFeatureSelector, createSelector } from '@ngrx/store';
import { Book } from '../models/book.model';
import { BookState } from './books.state';

export const selectBookState = createFeatureSelector<BookState>('book');
export const selectBooksList = createSelector(selectBookState, (state) => state.books);
export const selectBookById = (id: number) => createSelector(selectBooksList, (books) => books.find((x) => x.id === id));
