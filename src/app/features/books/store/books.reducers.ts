import { createReducer, on } from '@ngrx/store';
import { Book } from '../models/book.model';
import { addBook, deleteBook, deleteBookSuccess, getBooksSuccess, updateBook } from './books.actions';
import { BookState } from './books.state';

export const initialState: BookState = {
  books: [],
};

export const booksReducer = createReducer(
  initialState,
  on(addBook, (state, { book }) => {
    return { ...state, books: [...state.books, book] };
  }),
  on(deleteBookSuccess, (state, { book }) => {
    return {
      ...state,
      books: state.books.filter((b: Book) => b.id !== book.id),
    };
  }),
  on(getBooksSuccess, (state, { books }) => {
    return { ...state, books };
  })
);
