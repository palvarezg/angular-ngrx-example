import { createAction, props } from '@ngrx/store';
import { Book } from '../models/book.model';

export const booksKey = '[Books]';

export const addBook = createAction(`${booksKey} Add Book`, props<{ book: Book }>());
export const addBookSuccess = createAction(`${booksKey} Add Book Success`);
export const addBookError = createAction(`${booksKey} Add Book Error`);

export const getBooks = createAction(`${booksKey} Get Books`);
export const getBooksSuccess = createAction(`${booksKey} Get Books Success`, props<{ books: Book[] }>());
export const getBooksError = createAction(`${booksKey} Get Books Error`);

export const updateBook = createAction(`${booksKey} Update Book`, props<{ id: number; book: Book }>());
export const updateBookSuccess = createAction(`${booksKey} Update Book Success`);
export const updateBookError = createAction(`${booksKey} Update Book Error`);

export const deleteBook = createAction(`${booksKey} Delete Book`, props<{ book: Book }>());
export const deleteBookSuccess = createAction(`${booksKey} Delete Book Success`, props<{ book: Book }>());
export const deleteBookError = createAction(`${booksKey} Delete Book Error`);
