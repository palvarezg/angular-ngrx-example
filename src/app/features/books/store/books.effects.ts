import { Actions, createEffect, ofType } from '@ngrx/effects';
import { BookService } from '../services/books.service';
import { Injectable, inject } from '@angular/core';
import {
  addBook,
  addBookSuccess,
  deleteBook,
  deleteBookError,
  deleteBookSuccess,
  getBooks,
  getBooksSuccess,
} from './books.actions';
import { catchError, map, mergeMap, switchMap } from 'rxjs';
import { Book } from '../models/book.model';

@Injectable()
export class BooksEffects {
  constructor(private readonly actions$: Actions, private readonly booksService: BookService) {}
  getBooks$ = createEffect(() =>
    this.actions$.pipe(
      ofType(getBooks.type),
      switchMap(() => this.booksService.getBooks()),
      map((books: Book[]) => getBooksSuccess({ books }))
    )
  );

  addBook$ = createEffect(() =>
    this.actions$.pipe(
      ofType(addBook.type),
      switchMap(({ book }) => this.booksService.addBook(book)),
      map((book: Book) => addBookSuccess())
    )
  );

  deleteBook$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(deleteBook.type),
      mergeMap(({ book }) =>
        this.booksService.deleteBook(book).pipe(
          map(() => deleteBookSuccess({ book })),
          catchError(() => [deleteBookError()])
        )
      )
    );
  });
}
